import * as React from 'react';

import DarkTheme from './src/DarkTheme';

export default function App() {
  return (
    <DarkTheme />
  );
}
