import { DarkTheme, DefaultTheme } from "@react-navigation/native";

let listData = [];

for (let i = 1; i <= 5; i++) {
  listData.push({
    id: i,
    img: `https://picsum.photos/640/360?random=${i}`,
    title: `Title ${i}`,
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  });
}

const availableThemes = [
  {
    id: 1,
    name: "System Default",
    isSelected: true,
  },
  {
    id: 2,
    name: "Light",
    isSelected: false,
  },
  {
    id: 3,
    name: "Dark",
    isSelected: false,
  },
];

const CustomLightTheme = {
  ...DefaultTheme,
  dark: false,
  colors: {
    ...DefaultTheme.colors,
    primary: "white",
    background: "#F8F9FB",
    card: "#0178D4",
    border: "#DADBDF",
  },
};

const CustomDarkTheme = {
  ...DarkTheme,
  dark: true,
  colors: {
    ...DarkTheme.colors,
    primary: "#202020",
    background: "#394143",
    card: "#202020",
    border: "#626368",
  },
};

const getSelectedTheme = (themes) => themes?.filter((item) => item.isSelected);

export {
  availableThemes,
  CustomLightTheme,
  CustomDarkTheme,
  listData,
  getSelectedTheme,
};
