import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { useColorScheme } from 'react-native-appearance';

import CustomDrawer from './CustomDrawer';
import StackScreen from './StackScreen';
import ThemingContext from './ThemingContext';
import { CustomLightTheme, CustomDarkTheme, getSelectedTheme } from '../services';
import SwitchTheme from './SwitchTheme';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const StackNavigator = props => {
    const { name, title } = props;

    return (
        <Stack.Navigator>
            <Stack.Screen
                name={name}
                component={StackScreen}
                options={{
                    title: title,
                    headerTitleAlign: 'center',
                    headerTintColor: 'white'
                }}
            />
        </Stack.Navigator>
    );
};

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator
            drawerContent={props => <CustomDrawer {...props} />}
            drawerStyle={{
            backgroundColor: '#F8F9FB'
            }}
        >
            <Drawer.Screen
                name='First'
                children={() => <StackNavigator name='First_Stack' title='First' />}
            />
            <Drawer.Screen
                name='Second'
                children={() => <StackNavigator name='Second_Stack' title='Second' />}
            />
            <Drawer.Screen
                name='Third'
                children={() => <StackNavigator name='Third_Stack' title='Third' />}
            />
            <Drawer.Screen
                name='Fourth'
                children={() => <StackNavigator name='Fourth_Stack' title='Fourth' />}
            />
        </Drawer.Navigator>
    )
};

const Navigation = () => {
    const { themes } = useContext(ThemingContext);
    const selectedTheme = getSelectedTheme(themes);
    let currentTheme = null;

    if (selectedTheme?.[0]?.id === 1) {
        const deviceTheme = useColorScheme();
        if (deviceTheme === 'light') {
            currentTheme = CustomLightTheme;
        } else {
            currentTheme = CustomDarkTheme;
        }
    } else if (selectedTheme?.[0]?.id === 2) {
        currentTheme = CustomLightTheme;
    } else {
        currentTheme = CustomDarkTheme;
    }

    return (
        <NavigationContainer theme={currentTheme}>
            <Tab.Navigator>
                <Tab.Screen name="First_Stack" component={StackScreen} />
                <Tab.Screen name="Second_Stack" component={StackScreen} />
                <Tab.Screen name="Settings" component={SwitchTheme} />
            </Tab.Navigator>
        </NavigationContainer>
    );
};

export default Navigation;