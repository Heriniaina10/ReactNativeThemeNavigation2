import React, { useContext } from "react";
import { View, Text, Pressable, StyleSheet } from "react-native";
import { useTheme } from "@react-navigation/native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import ThemingContext from "./ThemingContext";

export default () => {
    const { dark, colors } = useTheme();
    const { themes, updateTheme } = useContext(ThemingContext);


    return (
        <View>
            <Text style={[ styles.themeLabel, { color: colors.text }]}>
                Select Theme:
            </Text>

            <View style={[ styles.themeButtonsContainers, { borderTopColor: colors.border }]} >
                {
                    themes.map((theme) => {
                        return (
                            <Pressable
                                key={theme?.id}
                                style={styles.btn}
                                onPress={() => updateTheme(theme?.id)}
                            >
                                <Text style={[ styles.btnText, { color: colors.text }]}>
                                    {theme.name}
                                </Text>

                                <View style={[ styles.radioButtonContainer, { borderColor: dark ? colors.border : colors.card} ]}>
                                    {theme.isSelected && (
                                        <View style={[ styles.fillView, { backgroundColor: dark ? colors.text : colors.card }]} />
                                    )}
                                </View>
                            </Pressable>
                        );
                    })
                }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingBottom: wp(3),
    },

    titleContainer: {
      paddingVertical: wp(6),
      marginBottom: wp(4),
      alignItems: "center",
      borderBottomWidth: wp(0.3),
    },

    titleText: {
      fontSize: wp(4),
      color: "#393D40",
      fontWeight: "bold",
      paddingTop: wp(4),
    },

    activeContainer: {
      borderLeftWidth: wp(1.06),
      backgroundColor: "rgba(0,0,0,0.2)",
      borderRadius: wp(0.8),
      marginTop: 0,
    },

    activeText: {
      color: "#393D40",
      backgroundColor: "transparent",
      fontWeight: "700",
      fontSize: wp(4),
    },

    inActiveContainer: {
      borderLeftWidth: wp(1.06),
      backgroundColor: "transparent",
      borderRadius: wp(0.8),
      marginTop: 0,
    },

    inactiveText: {
      color: "#393D40",
      backgroundColor: "transparent",
      fontWeight: "600",
      fontSize: wp(4),
    },

    themeLabel: {
      fontWeight: "bold",
      fontSize: wp(5),
      padding: wp(3),
    },

    btn: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      padding: wp(3),
    },

    themeButtonsContainers: {
      borderTopWidth: wp(0.3),
      paddingTop: wp(2),
    },

    btnText: {
      fontWeight: "bold",
      fontSize: wp(4),
    },

    radioButtonContainer: {
      height: wp(5.5),
      width: wp(5.5),
      borderRadius: wp(2.75),
      borderWidth: wp(0.4),
      justifyContent: "center",
      alignItems: "center",
    },

    fillView: {
      height: wp(3),
      width: wp(3),
      borderRadius: wp(1.5),
    },
});

