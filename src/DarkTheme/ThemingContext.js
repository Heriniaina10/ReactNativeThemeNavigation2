import { createContext } from 'react';

const ThemingContext = createContext();

export default ThemingContext;