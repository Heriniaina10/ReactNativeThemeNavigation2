import React, { useEffect } from 'react';
import { Text, View, FlatList, StyleSheet, StatusBar, Image, Pressable } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useTheme, useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { SafeAreaView } from 'react-native-safe-area-context';

import { listData } from '../services';

const renderItem = (item, colors) => {
    return (
        <View style={[styles.itemContainer, { backgroundColor: colors.primary, borderColor: colors.border }]}>
            <Image
                source={{ uri: item?.img }}
                style={[styles.image, { borderColor: colors.border }]}
            />
            <View style={[styles.itemSeparator, { backgroundColor: colors.border  }]} />
            <View style={styles.titleDescHolder}>
                <Text style={[styles.title, { color: colors.text }]}>{item?.title}</Text>
                <Text style={[styles.description, { color: colors.text }]} textBreakStrategy='simple'>
                    {item?.description}
                </Text>
            </View>
        </View>
    );
};

const itemSeparatorComponent = () => <View style={styles.separator} />;

const StackScreen = () => {
const navigation = useNavigation();
const { colors } = useTheme();

useEffect(() => {
    navigation.setOptions({
    headerLeft: () => (
        <Pressable
        onPress={() => navigation.toggleDrawer()}
        style={styles.menuBtn}
        >
        <Icon name='menu' size={wp(7)} color='white' />
        </Pressable>
    )
    });
}, [navigation]);

return (
        <SafeAreaView
            edges={['bottom']}
            style={[styles.container, { backgroundColor: colors.background }]}
        >
            <StatusBar
                translucent
                backgroundColor='transparent'
                barStyle='light-content'
            />

            <FlatList
                keyExtractor={item => item?.id?.toString()}
                data={listData}
                contentContainerStyle={styles.listContainerStyle}
                renderItem={({ item }) => renderItem(item, colors)}
                ItemSeparatorComponent={itemSeparatorComponent}
                showsVerticalScrollIndicator={false}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    menuBtn: {
        marginLeft: wp(2)
    },

    separator: {
        height: wp(4),
        backgroundColor: 'transparent'
    },

    listContainerStyle: {
        paddingHorizontal: wp(5),
        paddingTop: wp(5)
    },

    itemContainer: {
        borderRadius: wp(3),
        borderWidth: wp(0.5),
        flexDirection: 'row',
        alignItems: 'flex-start'
    },

    image: {
        width: wp(25),
        height: wp(25),
        margin: wp(3),
        marginRight: 0,
        borderRadius: wp(2),
        borderWidth: wp(0.3)
    },

    itemSeparator: {
        width: wp(0.3),
        height: '100%',
        marginHorizontal: wp(2.5)
    },

    titleDescHolder: {
        flex: 1,
        margin: wp(3),
        marginLeft: 0
    },

    title: {
        fontSize: wp(5),
        flex: 1,
        lineHeight: wp(6)
    },

    description: {
        flex: 1,
        marginTop: wp(1.5),
        fontSize: wp(3.5)
    }
});

export default StackScreen;