import React, { useState, useEffect, useRef } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ThemingContext from './ThemingContext';
import { availableThemes, getSelectedTheme } from '../services';

const ThemingProvider = props => {
    const [themes, updateAppTheme] = useState([...availableThemes]);
    const executeOnUpdate = useRef(false);

    useEffect(() => {
        const fetchPersistedTheme = async () => {
            try {
                let selectedTheme = await AsyncStorage.getItem('selectedTheme');
                executeOnUpdate.current = true;

                if (selectedTheme) {
                    selectedTheme = JSON.parse(selectedTheme);

                    updateAppTheme(() => {
                        return themes.map(themeEle => {
                            if (themeEle?.id === selectedTheme?.id) {
                                return { ...themeEle, isSelected: true };
                            }

                            return { ...themeEle, isSelected: false };
                        });
                    });
                } else {
                    updateAppTheme([...themes]);
                }
            } catch (err) {
                alert(err?.message || 'Something went wrong!');
            }
        };

        fetchPersistedTheme();
    }, []);

    useEffect(() => {
        const updatePersistedTheme = async () => {
            try {
                const selectedItem = getSelectedTheme(themes);
                if (selectedItem?.length !== 0) {
                    await AsyncStorage.setItem('selectedTheme', JSON.stringify(selectedItem?.[0]));
                }
            } catch (error) {
                alert(err?.message || 'Something went wrong!');
            }
        };

        if (executeOnUpdate.current) {
            updatePersistedTheme();
        }
    }, [themes]);

    return (
        <ThemingContext.Provider
            value={{
                themes,
                updateTheme: id => {
                    updateAppTheme(prevState => {
                        return prevState.map(item => {
                            if (item?.id === id) {
                                item.isSelected = true;
                            } else {
                                item.isSelected = false;
                            }

                            return item;
                        });
                    })
                }
            }}
        >
            {props.children}
        </ThemingContext.Provider>
    )
};

export default ThemingProvider;