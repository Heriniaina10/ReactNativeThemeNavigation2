import React from 'react';
import { AppearanceProvider } from 'react-native-appearance';

import Navigation from './navigation';
import ThemingProvider from './ThemingProvider';

const DarkTheme = () => {
  return (
    <AppearanceProvider>
      <ThemingProvider>
        <Navigation />
      </ThemingProvider>
    </AppearanceProvider>
  )
};

export default DarkTheme;